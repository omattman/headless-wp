import React, { Component } from 'react';
import fetch from "isomorphic-unfetch";
import { Config } from '../config';

export default class MainNav extends Component {
  state = {
    links: []
  }

  async componentWillMount() {
    const mainMenuRes = await fetch(`${Config.apiUrl}/wp-json/menus/v1/menus/main-nav`);
    const mainMenu = await mainMenuRes.json();
    this.setState({
      links: mainMenu.items
    });
  }
  render() {
    const { links } = this.state;
    return (
      <nav>
        {links.map(link => {
          // Save for later
          return <a>{link.title}</a>
        })}
      </nav>
    )
  }
}
