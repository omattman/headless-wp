import Layout from "../components/Layout.js";
import React, { Component } from 'react';
import PostIndex from "../components/PostIndex";
import fetch from "isomorphic-unfetch";
import { Config } from '../config';

export default class PostIndexPage extends Component {

  render() {
    const { posts } = this.props;
    return (
      <Layout index={false}>
        <h1>Post Index</h1>
        <PostIndex limit={20} />
      </Layout>
    )
  }
}
